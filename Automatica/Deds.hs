-- | Discrete Event Dynamical Systems and automatons in all their power.
--   Functions available to create automatons, load them from text
--   definitions and print them as Graphviz *dot* files ready for
--   picture generation.

module Automatica.Deds (
    -- * Types and classes
    Automa (..), Behavior, cartProdS,
    
    -- * Automaton analysis
    gamma, gammas, trans, transs, star, closure, isClosedToPrefix, lang,
    langM, isInitial, isMarked, isAc, isCoac, canReach, isDeadlock,
    isLivelock, ac, coac, hasLocks, trim, compl, prod, parall,
    
    -- * Automaton setup
    addState, addStates, addEvent, addEvents, addTrans, addTranss,
    addMarked, addMarkeds, setInitial, makeSingleState, makeAutoma1,
    makeAutoma2,
    
    -- * Input and output
    save, load, saveAsGraphviz
    
) where 

import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.List as L
                (nub, inits, (\\), sort, intersect, union)
import qualified Data.Foldable as F
import Data.Maybe
import System.IO

------------
-- * Types and classes


-- | Represents an automaton in the form
--   /G = (X, E, f, Gamma, x_0, X_m)/
--
--   NB: we're not including Gamma in the definition of Automaton
data Automa st ev = Automa {
    states     :: S.Set st,         -- ^ /X/, set of states
    events     :: S.Set ev,         -- ^ /E/, set of events
    mapTrans   :: M.Map (st,ev) st, -- ^ not /f/, but a map of
                                    --   transitions (see 'trans')
    initial    :: st,               -- ^ /x_0/, initial state
    marked     :: S.Set st          -- ^ /X_m/, set of marked states
} deriving (Show, Read, Eq, Ord)

type Behavior ev = [ev]


-- | Cartesian product for sets
cartProdS :: (Ord a) => S.Set a -> S.Set a -> S.Set (a,a)
cartProdS a b = S.fromList [ (x,y) | x <- S.toList a, y <- S.toList b ]


-----------------------
-- * Automaton analysis


-- | Indicator function of active states for an automaton
gamma :: (Ord st, Ord ev)
    => (st,ev)                 -- ^ state-event pair of interest
    -> Automa st ev
    -> Bool                    -- ^ 'True' if 'ev' is active for 'st'
gamma (s,e) au = M.member (s,e) $ mapTrans au

-- | Returns the set of active events for a state of a given 'Automa'
gammas :: (Ord st, Ord ev) => st -> Automa st ev -> S.Set ev
gammas s au = S.filter (\e -> gamma (s,e) au) $ events au

-- | Returns the list of events that are actually active (i.e. they
--   appear in an automaton's transition map).
aEvents :: (Ord ev) => M.Map (st,ev) st -> S.Set ev
aEvents t = S.fromList [ e | ((s,e),s') <- M.toList t ]

-- | Transition function: where do I get if 'ev' happens when in 'st'?
trans :: (Ord st, Ord ev) => (st, ev) -> Automa st ev -> Maybe st
trans (s,e) au = M.lookup (s,e) $ mapTrans au

-- | Like 'trans', but to be used if you're sure it's a valid
--   transition (i.e. saves you a 'fromJust').
trans' :: (Ord st, Ord ev) => (st, ev) -> Automa st ev -> st
trans' (s,e) au = fromJust $ M.lookup (s,e) $ mapTrans au

-- | Like 'trans', but takes a full string of events.
transs :: (Ord st, Ord ev) => st -> Behavior ev -> Automa st ev -> Maybe st
transs s []     au = Just s
transs s (e:es) au = case trans (s,e) au of
                        Just s' -> transs s' es au
                        Nothing -> Nothing

-- | The * operator for sets: returns the set of /all possible/ strings
--   of /all possible/ lengths based on elements of the param. This set is
--   infinite.
--
-- >>> star "abc"
-- ["","a","b","c","aa","ab", ...
star :: [a] -> [[a]]
star ss = []:(concat $ scanl1 cProd (repeat elems))
    where elems = [ [x] | x <- ss ]
          cProd xs ys = [ x ++ y | x <- xs, y <- ys ]

-- | Closure with respect to prefix of a set (used with languages).
--   /Watch out for infinite lists, I fear this keeps the whole list in
--   memory for 'nub'/.
--
-- >>> closure ["abc","cba"]
-- ["","a","ab","abc","c","cb","cba"]
closure :: (Eq ev) => [Behavior ev] -> [Behavior ev]
closure bs = L.nub $ concatMap L.inits bs

-- | Merely: true if closure of /L/ is equal to /L/
isClosedToPrefix :: (Eq ev) => [Behavior ev] -> Bool
isClosedToPrefix x = closure x == x

-- | The language of an automaton. All possible accepted strings of
--   events. This might be infinite of course.
lang :: (Ord st, Ord ev) => Automa st ev -> [Behavior ev]
-- this is the totally inefficient, naive, scholastic implementation
--lang au = [ es | es <- star e, transs i es au /= Nothing]
    --where e = events au
          --i = initial au
lang au = map fst $ langWstates au

-- | Like 'lang', but each string paired with the state it brings to.
langWstates :: (Ord st, Ord ev) => Automa st ev -> [(Behavior ev,st)]
langWstates au = ini: helper [ini] au
    where ini = ([],initial au)
          helper trs au = ttrs ++ helper ttrs au
            where ttrs = concatMap develop trs
                  develop (ex,s) = [ (ex ++ [e], trans' (s,e) au)
                                        | e <- S.toList $ gammas s au ]

-- | The /marked/ language of an automaton. Might be infinite.
langM :: (Ord st, Ord ev) => Automa st ev -> [Behavior ev]
-- this too is a totally inefficient, naive, scholastic implementation
--langM au = [ es | es <- star e, isM $ transs i es au]
    --where e = events au
          --i = initial au
          --isM Nothing  = False
          --isM (Just x) = isMarked x au
langM au = map fst $ filter (\(_,s) -> isMarked s au) $ langWstates au

-- | True if the state is initial for that automaton.
isInitial :: (Ord st) => st -> Automa st ev -> Bool
isInitial st au = st == initial au

-- | True if a state is marked.
isMarked :: (Ord st) => st -> Automa st ev -> Bool
isMarked st au = st `S.member` marked au

-- | /State "accessibility"/: a state is reachable if there is a string
--   that brings the automaton from initial state to this one.
isAc :: (Ord st, Ord ev) => st -> Automa st ev -> Bool
isAc s' au = canReach (initial au) s' au

-- | True if a state can reach another for some string of events.
canReach :: (Ord st, Ord ev)
    => st                   -- ^ starting state 's'
    -> st                   -- ^ arrival state 's''
    -> Automa st ev
    -> Bool
canReach s s' au = fst $ canReachExcluding s s' au S.empty

-- | True if a state can reach another for some string of events,
--   excluding passing from specified states.
--   
--   This implementation starts the search from the starting state and
--   recursively checks each possible path.
--   
--   NOTE: /not sure of correctnes/, but (now) it passes back visited
--   states to exclude them when searching in other branches.
canReachExcluding :: (Ord st, Ord ev)
    => st                   -- ^ starting state 's'
    -> st                   -- ^ arrival state 's''
    -> Automa st ev
    -> S.Set st             -- ^ the states to be excluded
    -> (Bool, S.Set st)     -- ^ (result, visited states) - /note that
                            --   in general these are not the path/!
canReachExcluding s s' au ex
    | s == s'   = (True, ex)
    | otherwise = F.foldl hlp (False,S.insert s ex) rs
    where
        rs = neighbors s au S.\\ ex
        hlp (result,ex') x
            | result == True   = (True,ex')
            | otherwise = canReachExcluding x s' au $ S.insert x ex''
            where ex'' = ex' `S.union` ex

-- | Alternative implementation of 'canReachExcluding'. This
--   implementation goes backwards: it checks all non-excluded
--   states every time, so it's probably awful. However, it naturally
--   excludes searching into "dead end" branches. Maybe this is good for
--   very acyclic and/or richly branched (poorly interconnected) graphs.
--   
--   (Note: if we really want to go backwards, we could avoid the
--   problem of re-checking all the states every time by building a
--   reverse-tree with a Breadth First Search noting down parents.
--   That'd also let us find the shortest path. Yet with BFS we can just
--   find directly the shortest path. So a backwards search probably
--   makes no sense at all.)
canReachExcluding' :: (Ord st, Ord ev)
    => st                   -- ^ starting state 's'
    -> st                   -- ^ arrival state 's''
    -> Automa st ev
    -> S.Set st             -- ^ the states to be excluded
    -> Bool
canReachExcluding' start end au ex
    | start == end = True
    | otherwise    = F.any (\x -> canReachExcluding' start x au (S.insert end ex)) sx
        where sx = S.filter pred $ states au
              pred s = (not $ s `S.member` ex) && end `S.member` neighbors s au

-- | Returns states reachable from specified state in one step.
neighbors :: (Ord st, Ord ev) => st -> Automa st ev -> S.Set st
neighbors s au = S.map snd $ nbrsWevents s au

-- | Returns a state's 'neighbors', each coupled with the event that
--   brings to it.
nbrsWevents :: (Ord st, Ord ev) => st -> Automa st ev -> S.Set (ev,st)
nbrsWevents s au = S.map (\e -> (e,trans' (s,e) au) ) $  gammas s au

-- | /State "coaccessibility"/: a state is co-accessible if there's a
--   string that brings the automaton from this state to a marked one.
isCoac :: (Ord st, Ord ev) => st -> Automa st ev -> Bool
isCoac s au = F.any (\x -> canReach s x au) $ marked au

-- | True if the state is blocking (/deadlock/). Conditions:
--
--    * not marked;
--
--    * reachable;
--
--    * has no active events.
isDeadlock :: (Ord st, Ord ev) => st -> Automa st ev -> Bool
isDeadlock s au = (not $ isMarked s au) &&
                  isAc s au &&
                  (S.null $ gammas s au)

-- | True if a subset of states is blocking (/livelock/):
--
--   * no deadlocks (implies no marked states),
--
--   * at least one accessible,
--
--   * for every behaviour or string from one of these states, you
--     end in one of these states.
--
--   Note that it's sufficient to check the last condition for single
--   events, not every string -- easily demonstrable.
isLivelock :: (Ord st, Ord ev) => S.Set st -> Automa st ev -> Bool
isLivelock ss au =
    F.all (\x -> not $ isMarked x au) ss &&
    F.any (\x -> isAc x au) ss &&
    F.all (\x -> F.all (`S.member` ss) $ neighbors x au) ss

-- | Returns the reachable part of an automaton, removing all
--   unreachable states and their transitions (and any event no more
--   in use).
ac :: (Ord st, Ord ev) => Automa st ev -> Automa st ev
ac au@(Automa ss es ts is ms) =
    let ss' = S.filter (\s -> isAc s au) $ states au
        ts' = M.fromList [ ((s,e),s') | ((s,e),s') <- M.toList ts,
                                        s `S.member` ss',
                                        s' `S.member` ss' ]
        es' = aEvents ts'
        ms' = S.filter (\s -> isAc s au) ms
    in  Automa ss' es' ts' is ms'

-- | Returns the coaccessible part of an automaton, removing all non
--   coaccessible states and their transitions (and any event no more
--   in use).
coac :: (Ord st, Ord ev) => Automa st ev -> Automa st ev
coac au@(Automa ss es ts is ms) =
    let ss' = S.filter (\s -> isCoac s au) ss
        ts' = M.fromList [ ((s,e),s') | ((s,e),s') <- M.toList ts,
                                        s `S.member` ss',
                                        s' `S.member` ss' ]
        es' = aEvents ts'
        ms' = S.filter (\s -> isCoac s au) ms
    in  Automa ss' es' ts' is ms'

-- | Coac part removes all locks, so if /G == COAC(G)/, /G/ has no
--   locks.
hasLocks :: (Ord st, Ord ev) => Automa st ev -> Bool
hasLocks au = au /= coac au

-- | Trim of an automaton is /COAC(AC(G))/. If /G = trim G/ then /G/ is
--   said to be /trim/ or /reduced/.
trim :: (Ord st, Ord ev) => Automa st ev -> Automa st ev
trim = coac.ac

-- | Complement of an automaton. How to complement:
--
--   1) add an unmarked /dead/ or /dump/ state /x_d/;
--   
--   2) define transitions, where missing, to /x_d/, included from
--      /x_d/;
--   
--   3) invert markings (after adding unmarked dump state!).
--   
--   NOTE: if /G/ is /trim/, and marks /L/, /compl(G)/ marks /E*\L/.
--   See theory for why it must be trim (not really sure why).
compl :: (Ord st, Ord ev)
    => st               -- ^ the dead state
    -> Automa st ev     -- ^ /G/
    -> Automa st ev     -- ^ /compl(G)/
compl dSt (Automa ss es ts i ms) =
    let ss' = S.insert dSt ss
        ms' = ss' S.\\ ms
        au' = Automa ss' es ts i ms'
        cts = concatMap mkComplTranss $ S.toList ss'
        mkComplTranss s = map (\e -> (s,e,dSt)) $ unactiveFor s
        unactiveFor s = S.toList $ es S.\\ gammas s au'
    in addTranss cts au'
        
-- | /Product/ of two automatons.
--   
--   /lang G1×G2/ equals intersection of /lang G1/ and /lang G2/. Same
--   for marked language.
prod :: (Ord st, Ord ev) => Automa st ev -> Automa st ev -> Automa (st,st) ev
prod a1 a2 = ac $ Automa ss es ts i ms
    where
        ss = states a1 `cartProdS` states a2
        es = S.intersection (events a1) (events a2)
        ts = M.fromList $ concatMap prodTranss $ S.toList ss
        i  = (initial a1, initial a2)
        ms = marked a1 `cartProdS` marked a2
        -- builds a list of transition tuples for a state of ss
        prodTranss (s1,s2) = catMaybes [ pTrans (s1,s2) e | e <- S.toList es ]
        -- builds the Maybe transition tuple from a state with event e
        pTrans (s1,s2) e
            | e `S.member` commonEvs = 
                Just (((s1,s2), e), (trans' (s1,e) a1, trans' (s2,e) a2))
            | otherwise              = Nothing
            where
                commonEvs = S.intersection (gammas s1 a1) (gammas s2 a2)

-- | Parallel composition of two automatons. If /all/ events from states
--   are in common it's like product, if /none/ are there's no
--   synchronism at all.
parall :: (Ord st, Ord ev) => Automa st ev -> Automa st ev -> Automa (st,st) ev
parall a1 a2 = ac $ Automa ss es ts i ms
    where
        ss = states a1 `cartProdS` states a2
        es = S.union (events a1) (events a2)
        ts = M.fromList $ concatMap parallTranss $ S.toList ss
        i  = (initial a1, initial a2)
        ms = marked a1 `cartProdS` marked a2
        -- builds a list of transition tuples for a state of ss
        parallTranss (s1,s2) = catMaybes [ pTrans (s1,s2) e | e <- S.toList es ]
        -- builds the Maybe transition tuple from a state with event e
        pTrans (s1,s2) e
            | eActiveForS1 && eActiveForS2 =
                Just (((s1,s2), e), (trans' (s1,e) a1, trans' (s2,e) a2))
            | eActiveForS1 && e `notIn` a2 =
                Just (((s1,s2), e), (trans' (s1,e) a1, s2))
            | eActiveForS2 && e `notIn` a1 =
                Just (((s1,s2), e), (s1, trans' (s2,e) a2))
            | otherwise    = Nothing
            where
                eActiveForS1 = gamma (s1,e) a1
                eActiveForS2 = gamma (s2,e) a2
                notIn e au   = not $ e `S.member` events au




--------------------
-- * Automaton setup


addState :: (Ord st) => st -> Automa st ev -> Automa st ev
addState st (Automa s e t i m) = Automa (S.insert st s) e t i m

addStates :: (Ord st) => [st] -> Automa st ev -> Automa st ev
addStates ss au = foldr addState au ss

addEvent :: (Ord ev) => ev -> Automa st ev -> Automa st ev
addEvent ev (Automa s e t i m) = Automa s (S.insert ev e) t i m

addEvents :: (Ord ev) => Behavior ev -> Automa st ev -> Automa st ev
addEvents ss au = foldr addEvent au ss

addTrans :: (Ord st, Ord ev) => (st,ev,st) -> Automa st ev -> Automa st ev
addTrans (x,y,z) (Automa s e t i m) = Automa s e t' i m
                where t' = (M.insert (x,y) z t)

addTranss :: (Ord st, Ord ev) => [(st,ev,st)] -> Automa st ev -> Automa st ev
addTranss ts au = foldr addTrans au ts

addMarked :: (Ord st) => st -> Automa st ev -> Automa st ev
addMarked st (Automa s e t i m) = Automa s e t i (S.insert st m)

addMarkeds :: (Ord st) => [st] -> Automa st ev -> Automa st ev
addMarkeds ss au = foldr addMarked au ss

setInitial :: st -> Automa st ev -> Automa st ev
setInitial st (Automa s e t i m) = Automa s e t st m



makeSingleState :: (Ord st) => st -> Automa st ev
makeSingleState i = Automa (S.singleton i) S.empty M.empty i S.empty


-- | For quick/ghci testing purposes. Returns this automaton:
--
-- > Automa {
-- >    states = [2,3,4,1],
-- >    events = "abc",
-- >    mapTrans = fromList [((1,'a'),2),
-- >                         ((1,'c'),4),
-- >                         ((2,'b'),3),
-- >                         ((3,'a'),4),
-- >                         ((4,'a'),2)],
-- >    initial = 1,
-- >    marked = [2,3]
-- > }
makeAutoma1 :: Automa Int Char
makeAutoma1 = addStates [2,3,4]
            $ addEvents ['a','b','c']
            $ addTranss [(1,'a',2), (2,'b',3), (3,'a',4), (1,'c',4), (4,'a',2)]
            $ addMarkeds [2,3]
            $ makeSingleState 1

makeAutoma2 :: Automa Int Char
makeAutoma2 = addStates [5,6,7]
            $ addTranss [(3,'c',5),(5,'c',4),(5,'a',6),(6,'b',7),(7,'c',6)]
            $ makeAutoma1



---------------------
-- * Input and output

save :: (Show st, Show ev) => String -> Automa st ev -> IO ()
save f au = do writeFile f $ show au

load :: String -> IO (Automa Int Char)
load f = do str <- readFile f
            let au = read str
            return au

saveAsGraphviz :: (Show st, Show ev, Ord st, Ord ev) => String -> Automa st ev -> IO ()
saveAsGraphviz f au = do writeFile f $ toGraphviz au

toGraphviz :: (Show st, Show ev, Ord st, Ord ev) => Automa st ev -> String
toGraphviz au = "digraph {\n\trankdir=LR\n\toverlap=false\n\tsplines=true\n"
    ++ concat (L.sort (sts ++ msts))
    ++ "\tinit [shape=point];\n"
    ++ "\tinit -> \""++ show (initial au) ++ "\";\n"
    ++ trs 
    ++ "}\n"
    where
        sts = S.toList $ S.map pSts $ states au S.\\ marked au
            where
                pSts x = "\t\"" ++ show x ++ "\" [shape=circle];\n"
        msts = S.toList $ S.map pMSts $ marked au
            where
                pMSts x = "\t\"" ++ show x ++ "\" [shape=doublecircle];\n"
        trs = concatMap pTrs $ M.toList $ mapTrans au
            where
                pTrs ((s,e),s') = "\t\"" ++ show s ++ "\" -> \"" ++ show s'
                    ++ "\" [label=\"" ++ show e ++ "\"];\n"
