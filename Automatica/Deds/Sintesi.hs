-- | Functions to synthesize DEDS: security or lock problems with /G/?
--   Let's create an automaton /R/ marking an acceptable language /K/
--   that has the following properties:
--   
--    - is subset of 'lang' /G/ (of course!)
--   
--    - 'isClosedToPrefix' (so 'lang' /R/ == 'langM' /R/)
--   
--    - 'isComplete'
--   
--    - 'isControllable' with respect to /G/
--   
--   Now /R/ is a realization of your control policy, just do
--   'prod' /R/ /G/ and see if it worked!

module Automatica.Deds.Sintesi (

    isComplete, isControllable
    
) where 

import Automatica.Deds

isComplete :: [Behavior ev] -> Bool
isComplete = undefined

isControllable :: [Behavior ev] -> Automa st ev -> Bool
isControllable = undefined
