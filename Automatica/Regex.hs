-- | Regular expressions.

module Automatica.Regex (
    -- * Classes
    Regex (..),
    
    toText    
    
) where 

import Data.Char

------------
-- * Classes

-- | This is the most elementary definition of regex. Every (?) regex
--   can be obtained from these elements, and also other constructions
--   like "one or more" and "optional". This is to keep close to lecture
--   notes.
data Regex a =
      Empty                     -- ^ no strings
    | Epsi                      -- ^ empty string
    | Lit  a                    -- ^ literal (single event)
    | Or   (Regex a) (Regex a)  -- ^ one regex or the other (+ in notes)
    | Cat  (Regex a) (Regex a)  -- ^ concatenation of two (justap. in notes)
    | Star (Regex a)            -- ^ Kleene closure of a regex (* in notes)

-- | Converts a 'Regex' to its text (string) representation.
toText :: (Show a) => Regex a -> String
toText Empty     = ""
toText Epsi      = "ε"
toText (Lit x)   = show x
toText (Or r s)  = "(" ++ toText r ++ "+" ++ toText s ++ ")"
toText (Cat r s) = toText r ++ toText s
toText (Star r)  = toText r ++ "*"
